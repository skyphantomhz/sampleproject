package aavn.library;

import aavn.library.entity.Categories;
import aavn.library.repository.CategoriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Created by lmchuc on 6/2/2017.
 */
@Component
public class DataLoader implements CommandLineRunner {

    private final CategoriesRepository categoriesRepository;

    @Autowired
    public DataLoader(CategoriesRepository bookRepository) {
        this.categoriesRepository = bookRepository;
    }

    @Override
    public void run(String... strings) throws Exception {
//        addBook("Markus Heitz", "Die Zwerge");
//        addBook("Nassim Nicholas Taleb", "Der Schwarze Schwan");
//        addBook("Stefan Merath", "Der Weg zum erfolgreichen Unternehmer");
//        addBook("Stephen R. Covey und Angela Roethe", "Die 7 Wege zur Effektivität");
//        addBook("Jens-Uwe Meyer", "Das Edison-Prinzip");
//        addBook("Joe Abercrombie ", "The first Law");
//        addBook("Daniel Kahneman", "Schnelles Denken Langsames Denken");
//        addBook("Napoleon Hill", "Denke nach und werde reich");
    }

    public Categories addCategory(String categoryName) {
        Categories category = new Categories();
        category.setCategoryName(categoryName);
        return addCategory(category);
    }

    public Categories addCategory(Categories category) {
        categoriesRepository.save(category);
        return category;
    }

}
