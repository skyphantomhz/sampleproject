package aavn.library.repository;

import aavn.library.entity.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by dnqduy on 7/7/2017.
 */
@Repository
public interface BooksRepository extends CrudRepository<Book, Long> {
}
