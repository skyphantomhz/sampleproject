package aavn.library.repository;

import aavn.library.entity.Barcodes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by dnqduy on 7/7/2017.
 */
@Repository
public interface BarcodesRepository extends CrudRepository<Barcodes, Long> {

}
