package aavn.library.repository;

import aavn.library.entity.Categories;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by lcdat on 7/7/2017.
 */
@Repository
public interface CategoriesRepository extends CrudRepository<Categories, Long> {
}
