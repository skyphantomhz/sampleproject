package aavn.library.controller;

import aavn.library.entity.Book;
import aavn.library.entity.Categories;
import aavn.library.service.BooksService;
import aavn.library.service.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by dnqduy on 7/7/2017.
 */
@RestController
@RequestMapping("/api/books")
public class BooksController {
    @Autowired
    private BooksService booksService;

    @GetMapping()
    public ResponseEntity getAllBooks() {
        return new ResponseEntity<>(booksService.findAllBooks(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity createNewBook(@Validated @RequestBody Book book) {
        Book todo = booksService.createNewBook(book);
        return new ResponseEntity<>(todo, HttpStatus.CREATED);
    }

    @PatchMapping(value = "/{id}")
    public ResponseEntity updateTodo(@PathVariable("id")  long id) {
        return new ResponseEntity<>(booksService.updateBook(id), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteTodo(@PathVariable("id")  long id) {
        booksService.deleteBook(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
