package aavn.library.controller;

import aavn.library.entity.Categories;
import aavn.library.service.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by lcdat on 7/7/2017.
 */
@RestController
@RequestMapping(value = "/api/categories")
public class CategoriesController {

    @Autowired
    private CategoriesService categoriesService;

    @GetMapping()
    public ResponseEntity getAllCategories() {
        return new ResponseEntity<>(categoriesService.findAllBCategories(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity createNewBook(@Validated @RequestBody Categories categories) {
        Categories todo = categoriesService.createNewCategory(categories);
        return new ResponseEntity<>(todo, HttpStatus.CREATED);
    }

    @PatchMapping(value = "/{id}")
    public ResponseEntity updateTodo(@PathVariable("id")  long id) {
        return new ResponseEntity<>(categoriesService.updateCategory(id), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteTodo(@PathVariable("id")  long id) {
        categoriesService.deleteCategory(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
