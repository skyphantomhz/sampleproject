package aavn.library.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by lmchuc on 6/2/2017.
 */

@Controller
public class RoutesController {

    @GetMapping(value = {"/", "/library","/404"})
    public String indexPage() {
        return "index";
    }
}
