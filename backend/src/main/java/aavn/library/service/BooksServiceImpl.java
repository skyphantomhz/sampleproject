package aavn.library.service;

import aavn.library.entity.Book;
import aavn.library.entity.Categories;
import aavn.library.repository.BooksRepository;
import aavn.library.repository.CategoriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dnqduy on 7/7/2017.
 */
@Service
@Transactional
public class BooksServiceImpl implements BooksService {

    @Autowired
    private BooksRepository booksRepository;

    @Override
    public Book createNewBook(Book book) {
        return booksRepository.save(book);
    }

    @Override
    public Book updateBook(long bookId) {
        return booksRepository.save(booksRepository.findOne(bookId));
    }

    @Override
    public void deleteBook(long bookId) {
        booksRepository.delete(bookId);
    }

    @Override
    public List<Book> findAllBooks() {
        List<Book> books = new ArrayList<>();
        booksRepository.findAll().forEach(books::add);
        return books;
    }
}
