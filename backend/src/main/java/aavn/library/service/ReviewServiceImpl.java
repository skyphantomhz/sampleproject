//package aavn.library.service;
//
//import aavn.library.entity.Review;
//import aavn.library.repository.ReviewRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import javax.transaction.Transactional;
//
///**
// * Created by dnqduy on 7/7/2017.
// */
//@Service
//@Transactional
//public class ReviewServiceImpl implements ReviewService{
//    @Autowired
//    private ReviewRepository reviewRepository;
//
//    @Override
//    public Review createNewReview(Review review) {
//        return reviewRepository.save(review);
//    }
//}
