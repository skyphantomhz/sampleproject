package aavn.library.service;

import aavn.library.entity.Categories;

import java.util.List;

/**
 * Created by lcdat on 7/7/2017.
 */
public interface CategoriesService {
    Categories createNewCategory(Categories categories);

    Categories updateCategory(long categoryId);

    void deleteCategory(long categoryId);

    List<Categories> findAllBCategories();
}
