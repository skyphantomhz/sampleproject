package aavn.library.service;

import aavn.library.entity.Barcodes;
import aavn.library.repository.BarcodesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by dnqduy on 7/7/2017.
 */
@Service
@Transactional
public class BarcodeServiceImpl implements BarcodeService {
    @Autowired
    private BarcodesRepository barcodesRepository;

    @Override
    public Barcodes addNewBarcode(Barcodes barcodes) {
        return barcodesRepository.save(barcodes);
    }
}
