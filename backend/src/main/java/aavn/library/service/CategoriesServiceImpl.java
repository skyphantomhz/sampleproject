package aavn.library.service;

import aavn.library.entity.Categories;
import aavn.library.repository.CategoriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lcdat on 7/7/2017.
 */

@Service
@Transactional
public class CategoriesServiceImpl implements CategoriesService {

    @Autowired
    private CategoriesRepository categoriesRepository;

    @Override
    public Categories createNewCategory(Categories categories) {
        return categoriesRepository.save(categories);
    }

    @Override
    public Categories updateCategory(long categoryId) {
        return categoriesRepository.save(categoriesRepository.findOne(categoryId));
    }

    @Override
    public void deleteCategory(long categoryId) {
        categoriesRepository.delete(categoryId);
    }

    @Override
    public List<Categories> findAllBCategories() {
        List<Categories> categories = new ArrayList<>();
        categoriesRepository.findAll().forEach(categories::add);
        return categories;
    }
}
