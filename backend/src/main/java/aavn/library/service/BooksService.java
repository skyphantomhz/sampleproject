package aavn.library.service;

import aavn.library.entity.Book;

import java.util.List;

/**
 * Created by dnqduy on 7/7/2017.
 */
public interface BooksService {
    Book createNewBook(Book book);

    Book updateBook(long bookID);

    void deleteBook(long bookID);

    List<Book> findAllBooks();
}
