package aavn.library.service;

import aavn.library.entity.Barcodes;

/**
 * Created by dnqduy on 7/7/2017.
 */
public interface BarcodeService {
    Barcodes addNewBarcode(Barcodes barcodes);
}
