package aavn.library.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by dnqduy on 7/7/2017.
 */
@Entity
public class Borrowings {
    @Id
    @GeneratedValue
    private Long borrowingId;

    private Date borrowDate;

    private Date dueDate;

    private boolean returnStatus;

    private Date returnDate;

    @ManyToOne
    @JoinColumn(name = "patronId")
    private Patron patron;

    @ManyToOne
    @JoinColumn(name = "barcode")
    private Barcodes barcodes;

    public Long getBorrowingId() {
        return borrowingId;
    }

    public void setBorrowingId(Long borrowingId) {
        this.borrowingId = borrowingId;
    }

    public Date getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public boolean isReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(boolean returnStatus) {
        this.returnStatus = returnStatus;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public Barcodes getBarcodes() {
        return barcodes;
    }

    public void setBarcodes(Barcodes barcodes) {
        this.barcodes = barcodes;
    }

    public Patron getPatron() {
        return patron;
    }

    public void setPatron(Patron patron) {
        this.patron = patron;
    }
}
