package aavn.library.entity;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by dnqduy on 7/7/2017.
 */
@Entity
public class Barcodes {
    @Id
    private Long barcode;

    @ManyToOne
    @JoinColumn(name = "bookID")
    private Book book;

    @OneToMany(mappedBy = "barcodes", cascade = CascadeType.ALL)
    private Set<Borrowings> borrowings;

    public Long getBarcode() {
        return barcode;
    }

    public void setBarcode(Long barcode) {
        this.barcode = barcode;
    }

    public Book getBook() {
        return book;
    }

    public void setBooks(Book book) {
        this.book = book;
    }

    public Set<Borrowings> getBorrowings() {
        return borrowings;
    }

    public void setBorrowings(Set<Borrowings> borrowings) {
        this.borrowings = borrowings;
    }
}
