package aavn.library.entity;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by lcdat on 7/7/2017.
 */
@Entity
public class Categories {

    @Id
    @GeneratedValue
    private Long categoryId;
    private String categoryName;

    @OneToMany(mappedBy = "categories", cascade = CascadeType.ALL)
    private Set<Book> book;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public Set<Book> getBook() {
        return book;
    }

    public void setBooks(Set<Book> book) {
        this.book = book;
    }

}
