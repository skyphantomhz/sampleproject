package aavn.library.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by dnqduy on 7/7/2017.
 */
@Entity
public class Patron {
    @Id
    @GeneratedValue
    private Long patronId;

    private String firstName;

    private String lastName;

    private Date dob;

    private int gender;

    private String address;

    private String phoneNumber;

    private String email;

    @OneToMany(mappedBy = "patron", cascade = CascadeType.ALL)
    private Set<Borrowings> borrowings;

    public Long getPatronId() {
        return patronId;
    }

    public void setPatronId(Long patronId) {
        this.patronId = patronId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Borrowings> borrowings() {
        return borrowings;
    }

    public void setBorrowings(Set<Borrowings> borrowings) {
        this.borrowings = borrowings;
    }
}
