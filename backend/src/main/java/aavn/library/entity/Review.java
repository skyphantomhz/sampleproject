package aavn.library.entity;

import javax.persistence.*;

/**
 * Created by dnqduy on 7/7/2017.
 */
@Entity
public class Review {
    @Id
    @GeneratedValue
    private Long reviewId;

    private int rating;

    private String comment;

    @ManyToOne
    @JoinColumn(name = "bookID")
    private Book book;

    public Long getReviewId() {
        return reviewId;
    }

    public void setReviewId(Long reviewId) {
        this.reviewId = reviewId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Book getBook() {
        return book;
    }

    public void setBooks(Book book) {
        this.book = book;
    }
}
