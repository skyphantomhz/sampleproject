package aavn.library.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by dnqduy on 7/7/2017.
 */
@Entity
public class Book {
    @Id
    @GeneratedValue
    private Long bookID;

    private String title;

    private String author;

    private String publisher;

    private Date publishDate;

    private String edition;

    private int inStock;

    private Date addedDate;

    private String coverImage;

    private String about;

    private String linkPreview;

    @ManyToOne
    @JoinColumn(name = "categoryID")
    private Categories categories;

    @OneToMany(mappedBy = "book", cascade = CascadeType.ALL)
    private Set<Barcodes> barcodes;

    @OneToMany(mappedBy = "book", cascade = CascadeType.ALL)
    private Set<Review> review;

    public Long getBookID() {
        return bookID;
    }

    public void setBookID(Long bookID) {
        this.bookID = bookID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public int getInStock() {
        return inStock;
    }

    public void setInStock(int inStock) {
        this.inStock = inStock;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getLinkPreview() {
        return linkPreview;
    }

    public void setLinkPreview(String linkPreview) {
        this.linkPreview = linkPreview;
    }

    public Categories getCategories() {
        return categories;
    }

    public void setCategories(Categories categories) {
        this.categories = categories;
    }

    public Set<Barcodes> getBarcodes() {
        return barcodes;
    }

    public void setBarcodes(Set<Barcodes> barcodes) {
        this.barcodes = barcodes;
    }

    public Set<Review> getReview() {
        return review;
    }

    public void setReviews(Set<Review> reviews) {
        this.review = review;
    }
}
